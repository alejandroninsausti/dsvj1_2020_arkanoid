#include "ball.h"

#include "Scenes/Gameloop/Paddle/paddle.h"
#include "Config/screen_size.h"
#include "Config/key_bindings.h"
#include "utility.h"

namespace arkanoid
{
	namespace gameloop
	{
		namespace ball
		{
			const int maxBalls = 5;
			int activeBalls;
			Ball balls[maxBalls];

			void Init()
			{
				activeBalls = 1;
								
				for (short i = 0; i < maxBalls; i++)
				{
					balls[i].pos.x = (paddle::player.rec.x + paddle::player.rec.width) / 2;
					balls[i].pos.y = paddle::player.rec.y - paddle::player.rec.height - balls[i].radius * 2;
					balls[i].radius = 0.013f * config::screenWidth;
					balls[i].color = RED;
					balls[i].speed.x = 0;
					balls[i].speed.y = 0;
					balls[i].maxSpeed = (config::screenWidth/100);
					i == 0 ? balls[i].isGrabbedByPlayer = true : balls[i].isGrabbedByPlayer = false;
				}
			}

			bool WillMoveWithinMap(Ball ball, bool xAxis)
			{
				if (xAxis)
				{
					if (ball.speed.x < 0)
					{
						return ball.pos.x - ball.radius + ball.speed.x > 0;
					}
					else
					{
						return ball.pos.x + ball.radius + ball.speed.x < config::screenWidth;
					}
				}
				else
				{
					if (ball.speed.y < 0)
					{
						return ball.pos.y - ball.radius + ball.speed.y > 0;
					}
					else
					{
						return ball.pos.y + ball.radius + ball.speed.y < config::screenHeight;
					}
				}
				//check if the player's position after moving will be inside of the map
				
			}

			bool BallRecCollision(Ball ball, Rectangle rec, bool xAxis)
			{
				if ((ball.pos.x + ball.radius >= rec.x && ball.pos.x - ball.radius <= rec.x + rec.width)
					&& (ball.pos.y - ball.radius <= rec.y + rec.height && ball.pos.y + ball.radius >= rec.y))
				{
					float smallestXDistance;
					GetAbsoluteValue(ball.pos.x + ball.radius - rec.x)
						< GetAbsoluteValue(ball.pos.x - ball.radius - (rec.x + rec.width)) ?
						smallestXDistance = GetAbsoluteValue(ball.pos.x + ball.radius - rec.x) :
						smallestXDistance = GetAbsoluteValue(ball.pos.x - ball.radius - (rec.x + rec.width));

					float smallestYDistance;
					GetAbsoluteValue(ball.pos.y + ball.radius - rec.y)
						< GetAbsoluteValue(ball.pos.y - ball.radius - (rec.y + rec.height)) ?
						smallestYDistance = GetAbsoluteValue(ball.pos.y + ball.radius - rec.y) :
						smallestYDistance = GetAbsoluteValue(ball.pos.y - ball.radius - (rec.y + rec.height));

					if (xAxis)
					{
						return smallestXDistance < smallestYDistance;
					}
					return smallestXDistance > smallestYDistance;
				}
				return false;
			}

			void RegulateSpeed(Ball& ball)
			{
				if (GetAbsoluteValue(ball.speed.x) > ball.maxSpeed)
				{
					ball.speed.x = ball.maxSpeed * (GetAbsoluteValue(ball.speed.x) / ball.speed.x);
				}

				if (GetAbsoluteValue(ball.speed.y) > ball.maxSpeed)
				{
					ball.speed.y = ball.maxSpeed * (GetAbsoluteValue(ball.speed.y) / ball.speed.y);
				}
			}

			void BounceAgainstPlayer(Ball& ball, bool xAxis)
			{
				if (xAxis)
				{
					ball.speed.x += (GetAbsoluteValue(ball.speed.x) / ball.speed.x) * (0.2f * ball.maxSpeed);
					ball.speed.x *= -1;
					ball.speed.y += paddle::player.speed;
					RegulateSpeed(ball);
				}
				else
				{
					ball.speed.y += (GetAbsoluteValue(ball.speed.y) / ball.speed.y) * (0.2f * ball.maxSpeed);
					ball.speed.y *= -1;
					ball.speed.x += paddle::player.speed;
					RegulateSpeed(ball);

					ball.pos.y += ball.speed.y * 2;
				}
			}

			void LaunchBall(Ball& ball)
			{
				ball.speed.x = paddle::player.speed / 3;
				ball.speed.y = -paddle::player.maxSpeed / 2;
				ball.isGrabbedByPlayer = false;
			}

			void MoveBall(Ball& ball)
			{
				if (ball.isGrabbedByPlayer)
				{
					ball.pos.x = paddle::player.rec.x + paddle::player.rec.width / 2;
					ball.pos.y = paddle::player.rec.y - paddle::player.rec.height * 1.25f - ball.radius;
				}
				else
				{
					if (WillMoveWithinMap(ball, true) && WillMoveWithinMap(ball, false))
					{
						if (BallRecCollision(ball, paddle::player.rec, true))
						{
							BounceAgainstPlayer(ball, true); //x Axis
						}
						else if (BallRecCollision(ball, paddle::player.rec, false))
						{
							BounceAgainstPlayer(ball, false); //y Axis
						}
					}
					else if (!WillMoveWithinMap(ball, true))
					{
						ball.speed.x *= -1;
					}
					else if (!WillMoveWithinMap(ball, false))
					{
						if (ball.pos.y <= config::screenHeight / 2)
						{
							ball.speed.y *= -1;
						}
						else
						{
							paddle::player.lives--;
							ball.isGrabbedByPlayer = true;
						}
					}
					RegulateSpeed(ball);
					ball.pos.x += ball.speed.x * GetFrameTime() * 35;
					ball.pos.y += ball.speed.y * GetFrameTime() * 35;
				}
			}

			void UpdateAll()
			{
				for (short i = 0; i < activeBalls; i++)
				{
					if (IsKeyPressed(config::keys.releaseBall) && balls[i].isGrabbedByPlayer)
					{
						LaunchBall(balls[i]);
					}
					MoveBall(balls[i]);
				}
			}

			void Draw()
			{
				for (short i = 0; i < activeBalls; i++)
				{
					DrawCircle(static_cast<int>(balls[i].pos.x), static_cast<int>(balls[i].pos.y), 
						balls[i].radius, balls[i].color);
				}
			}
		}
	}
}