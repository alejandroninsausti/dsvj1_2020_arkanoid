#ifndef BALL_H
#define BALL_H

#include "raylib.h"

namespace arkanoid
{
	namespace gameloop
	{
		namespace ball
		{
			struct Ball
			{
				Vector2 pos;
				float radius;
				Color color;
				Vector2 speed;
				float maxSpeed;
				bool isGrabbedByPlayer;
			};

			extern const int maxBalls;
			extern int activeBalls;
			extern Ball balls[];

			void Init();
			bool BallRecCollision(Ball ball, Rectangle rec, bool xAxis);
			void UpdateAll();
			void Draw();
		}
	}
}

#endif // !BALL_H
