#include "blocks.h"

#include "Config/screen_size.h"
#include "Scenes/Gameloop/Ball/ball.h"
#include "utility.h"

namespace arkanoid
{
	namespace gameloop
	{
		namespace blocks
		{			
			const Color intactColor = GREEN;
			const Color damagedColor = RED;
			const Color unbreakableColor = BLACK;

			Block blocks[blockColumns][blockRows];

			void Init(BLOCK_TYPE blockDesign[blockColumns][blockRows])
			{
				for (short i = 0; i < blockColumns; i++)
				{
					for (short j = 0; j < blockRows; j++)
					{
						blocks[i][j].rec.width = config::screenWidth / (blockColumns + 2);
						blocks[i][j].rec.height = config::screenHeight / (blockRows + 5);
						blocks[i][j].rec.x = blocks[i][j].rec.width * (i + i * 0.25f);
						blocks[i][j].rec.y = blocks[i][j].rec.height * (j + j * 0.25f);
						switch (blockDesign[i][j])
						{
						case arkanoid::gameloop::blocks::BLOCK_TYPE::UNEXISTANT:
							blocks[i][j].color = unbreakableColor;
							blocks[i][j].type = BLOCK_TYPE::UNEXISTANT;
							blocks[i][j].state = BLOCK_STATE::BROKEN;
							break;
						case arkanoid::gameloop::blocks::BLOCK_TYPE::UNBREAKABLE:
							blocks[i][j].color = unbreakableColor;
							blocks[i][j].type = BLOCK_TYPE::UNBREAKABLE;
							blocks[i][j].state = BLOCK_STATE::UNBREAKABLE;
							break;
						case arkanoid::gameloop::blocks::BLOCK_TYPE::BREAKABLE:
						default:
							blocks[i][j].color = intactColor;
							blocks[i][j].type = BLOCK_TYPE::BREAKABLE;
							blocks[i][j].state = BLOCK_STATE::INTACT;
							break;
						}
					}
				}
			}

			void DamageBlock(Block& block)
			{
				if (block.type == BLOCK_TYPE::BREAKABLE)
				{
					block.state = static_cast<BLOCK_STATE>(static_cast<int>(block.state) - 1);
					switch (block.state)
					{
					case arkanoid::gameloop::blocks::BLOCK_STATE::DAMAGED:
						block.color = damagedColor;
						break;
					case arkanoid::gameloop::blocks::BLOCK_STATE::INTACT:
						block.color = intactColor;
						break;
					default:
						break;
					}
				}
			}

			void UpdateAll()
			{
				for (short ballN = 0; ballN < ball::activeBalls; ballN++)
				{
					for (short i = 0; i < blockColumns; i++)
					{
						for (short j = 0; j < blockRows; j++)
						{
							if (static_cast<int>(blocks[i][j].state) > 0)
							{								
								if (ball::BallRecCollision(ball::balls[ballN], blocks[i][j].rec, true))
								{
									ball::balls[ballN].speed.x *= -1;
									DamageBlock(blocks[i][j]);
								}
								if (ball::BallRecCollision(ball::balls[ballN], blocks[i][j].rec, false))
								{
									ball::balls[ballN].speed.y *= -1;
									DamageBlock(blocks[i][j]);
								}
							}
						}
					}
				}
			}

			void Draw()
			{
				for (short i = 0; i < blockColumns; i++)
				{
					for (short j = 0; j < blockRows; j++)
					{
						if (static_cast<int>(blocks[i][j].state) != 0)
						{
							DrawRectangleRec(blocks[i][j].rec, blocks[i][j].color);
						}
					}
				}
			}

			bool ThereAreBlocksLeft()
			{
				for (short i = 0; i < blockColumns; i++)
				{
					for (short j = 0; j < blockRows; j++)
					{
						if (static_cast<int>(blocks[i][j].state) > 0)
						{
							return true;
						}
					}
				}			
				return false;
			}
		}
	}
}