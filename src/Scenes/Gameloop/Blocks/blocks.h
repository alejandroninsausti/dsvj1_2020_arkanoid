#ifndef BLOCKS_H
#define BLOCKS_H

#include "raylib.h"

namespace arkanoid
{
	namespace gameloop
	{
		namespace blocks
		{
			enum class BLOCK_STATE {UNBREAKABLE = -1, BROKEN, DAMAGED, INTACT};
			enum class BLOCK_TYPE {UNEXISTANT = -1, UNBREAKABLE, BREAKABLE};

			struct Block
			{
				Rectangle rec;
				Color color;
				BLOCK_TYPE type;
				BLOCK_STATE state;
			};

			const int blockColumns = 10;
			const int blockRows = 7;

			void Init(BLOCK_TYPE blockDesign[blockColumns][blockRows]);
			void UpdateAll();
			void Draw();
			bool ThereAreBlocksLeft();
		}
	}
}

#endif // !BLOCKS_H
