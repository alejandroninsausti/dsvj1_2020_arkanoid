#include "paddle.h"

#include <iostream>

#include "Config/screen_size.h"
#include "Config/key_bindings.h"
#include "utility.h"

namespace arkanoid
{
	namespace gameloop
	{
		namespace paddle
		{
			Paddle player;
			float timeSinceLevelStarted;

			void Init()
			{
				player.rec.height = 0.05f * config::screenHeight;
				player.rec.width = 0.175f * config::screenWidth;
				player.rec.y = config::screenHeight - player.rec.height / 2;
				player.rec.x = config::screenWidth / 2 - player.rec.width / 2;
				player.color = RED;
				player.maxSpeed = 0.013f * config::screenWidth;
				player.maxLives = 3;
				player.lives = player.maxLives;

				timeSinceLevelStarted = 0;
			}

			void DeInit()
			{
				for (short i = 0; i < 3; i++)
				{
					player.data.name[i] = '_';
				}
				player.data.score = 0;
				player.speed = 0;
			}

			bool WillMoveWithinMap(float speed)
			{
				//check if the player's position after moving will be inside of the map
				if (speed < 0)
				{
					return player.rec.x + speed > 0;
				}
				else
				{
					return player.rec.x + player.rec.width + speed < config::screenWidth;
				}
			}

			void MovePaddle()
			{
				float newSpeed = 0;

				if (IsKeyDown(config::keys.moveLeft))
				{
					newSpeed = -player.maxSpeed * GetFrameTime() * 50;
				}
				else if (IsKeyDown(config::keys.moveRight))
				{
					newSpeed = player.maxSpeed * GetFrameTime() * 50;
				}

				if (WillMoveWithinMap(newSpeed))
				{
					player.rec.x += newSpeed;
					player.speed = newSpeed;
				}				
			}

			void UpdatePaddle()
			{
				timeSinceLevelStarted += 1 * GetFrameTime();
				MovePaddle();
				player.data.score = static_cast<int>(player.lives / (timeSinceLevelStarted / 1000));
			}

			void Draw()
			{
				DrawRectangleRec(player.rec, player.color);
			}
		}
	}
}