#ifndef PADDLE_H
#define PADDLE_H

#include "raylib.h"

#include "Save/player_data.h"

using namespace arkanoid::save;

namespace arkanoid
{
	namespace gameloop
	{
		namespace paddle
		{
			struct Paddle
			{
				Rectangle rec;
				Color color;
				float speed;
				float maxSpeed;
				short lives;
				short maxLives;
				PlayerData data;
			};

			extern Paddle player;

			void Init();
			void DeInit();
			void UpdatePaddle();
			void Draw();
		}
	}
}

#endif // !PADDLE_H
