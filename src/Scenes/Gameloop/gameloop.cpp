#include "gameloop.h"

#include "raylib.h"

#include "Config/key_bindings.h"
#include "Scenes/Gameloop/Paddle/paddle.h"
#include "Scenes/Gameloop/Ball/ball.h"
#include "Scenes/Gameloop/Blocks/blocks.h"
#include "utility.h"
#include "Config/screen_size.h"
#include "Scenes/Levels/levels.h"

namespace arkanoid
{
	namespace gameloop
	{
		void Update()
		{
			paddle::UpdatePaddle();
			blocks::UpdateAll();
			ball::UpdateAll();
		}

		float letterSize;
		float letterSpacing;

		void InitInGameMenuVariables()
		{
			letterSize = config::screenWidth * 0.03f;
			letterSpacing = letterSize / 10;
		}

		void DrawLivesAndScore()
		{
			Rectangle scoreTextbox;
			InitTextbox(scoreTextbox, letterSize, 20, 1.75f, 8);
			Rectangle livesTextbox;
			InitTextbox(livesTextbox, letterSize, 11, 1, 9);
			
			DrawTextRec(GetFontDefault(), TextFormat("SCORE: %i", paddle::player.data.score),
				scoreTextbox, letterSize, letterSpacing, true, ORANGE);
			DrawTextRec(GetFontDefault(), TextFormat("LIVES: %i", paddle::player.lives),
				livesTextbox, letterSize, letterSpacing, true, ORANGE);			
		}

		void Draw()
		{
			BeginDrawing();
			paddle::Draw();
			ball::Draw();
			blocks::Draw();
			DrawLivesAndScore();
			ClearBackground(GRAY);
			EndDrawing();
		}

		void DrawWin()
		{
			Rectangle winTextbox;
			InitTextbox(winTextbox, letterSize * 3, 9, 5, 3);
			
			DrawTextRec(GetFontDefault(), "You Won!",
				winTextbox, letterSize * 3, letterSpacing, true, DARKGRAY);
		}

		void DrawLose()
		{
			Rectangle loseTextbox;
			InitTextbox(loseTextbox, letterSize * 3, 10, 5, 3);

			DrawTextRec(GetFontDefault(), "You Lose!",
				loseTextbox, letterSize * 3, letterSpacing, true, DARKGRAY);
		}

		void DrawInGameMenu()
		{
			Rectangle exitInstructions;
			InitTextbox(exitInstructions, letterSize, 55, 5, 5);

			BeginDrawing();
			paddle::Draw();
			ball::Draw();
			DrawTextRec(GetFontDefault(), "Press 'ESC' to return to menu or 'SPACE' to play again",
				exitInstructions, letterSize, letterSpacing, true, DARKGRAY);
			if (paddle::player.lives <= 0)
			{
				DrawLose();
			}
			else if (!blocks::ThereAreBlocksLeft())
			{
				DrawWin();
			}
			DrawLivesAndScore();
			ClearBackground(GRAY);
			EndDrawing();
		}

		void Gameloop()
		{
			paddle::Init();
			ball::Init();
			levels::LevelOne();
			InitInGameMenuVariables();

			do
			{
				Update();
				Draw();
			} while (!IsKeyPressed(config::keys.quit) && paddle::player.lives > 0 && blocks::ThereAreBlocksLeft());
			
			do
			{
				if (IsKeyPressed(config::keys.releaseBall))
				{
					Gameloop();
				}
				DrawInGameMenu();
			} while (!IsKeyPressed(config::keys.quit));
		}
	}
}