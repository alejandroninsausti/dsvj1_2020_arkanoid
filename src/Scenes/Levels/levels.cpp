#include "levels.h"

#include "Scenes/Gameloop/Blocks/blocks.h"

namespace arkanoid
{
	namespace levels
	{
		void LevelOne()
		{
			gameloop::blocks::BLOCK_TYPE blocks[gameloop::blocks::blockColumns][gameloop::blocks::blockRows];

			for (short i = 0; i < gameloop::blocks::blockColumns; i++)
			{
				for (short j = 0; j < gameloop::blocks::blockRows; j++)
				{
					blocks[i][j] = gameloop::blocks::BLOCK_TYPE::BREAKABLE;
				}
			}

			for (short i = 0; i < gameloop::blocks::blockColumns; i++)
			{
				blocks[i][0] = gameloop::blocks::BLOCK_TYPE::UNEXISTANT;
				blocks[i][gameloop::blocks::blockRows - 1] = gameloop::blocks::BLOCK_TYPE::UNEXISTANT;
			}

			for (short i = 0; i < gameloop::blocks::blockRows; i++)
			{
				blocks[0][i] = gameloop::blocks::BLOCK_TYPE::UNEXISTANT;
				blocks[gameloop::blocks::blockColumns - 1][i] = gameloop::blocks::BLOCK_TYPE::UNEXISTANT;
			}

			gameloop::blocks::Init(blocks);
		}

		void LevelTwo()
		{
			gameloop::blocks::BLOCK_TYPE blocks[gameloop::blocks::blockColumns][gameloop::blocks::blockRows];

			for (short i = 0; i < gameloop::blocks::blockColumns; i++)
			{
				for (short j = 0; j < gameloop::blocks::blockRows; j++)
				{
					blocks[i][j] = gameloop::blocks::BLOCK_TYPE::BREAKABLE;
				}
			}

			for (short i = 0; i < gameloop::blocks::blockColumns; i++)
			{
				blocks[i][0] = gameloop::blocks::BLOCK_TYPE::UNEXISTANT;
				blocks[i][1] = gameloop::blocks::BLOCK_TYPE::UNEXISTANT;
			}

			gameloop::blocks::Init(blocks);
		}

		void LevelThree()
		{
			gameloop::blocks::BLOCK_TYPE blocks[gameloop::blocks::blockColumns][gameloop::blocks::blockRows];

			for (short i = 0; i < gameloop::blocks::blockColumns; i++)
			{
				for (short j = 0; j < gameloop::blocks::blockRows; j++)
				{
					blocks[i][j] = gameloop::blocks::BLOCK_TYPE::BREAKABLE;
				}
			}

			for (short i = 0; i < gameloop::blocks::blockColumns; i++)
			{
				blocks[i][0] = gameloop::blocks::BLOCK_TYPE::UNEXISTANT;
				blocks[i][gameloop::blocks::blockRows - 1] = gameloop::blocks::BLOCK_TYPE::UNEXISTANT;
			}

			for (short i = 0; i < gameloop::blocks::blockRows; i++)
			{
				blocks[0][i] = gameloop::blocks::BLOCK_TYPE::UNEXISTANT;
				blocks[gameloop::blocks::blockColumns - 1][i] = gameloop::blocks::BLOCK_TYPE::UNEXISTANT;
			}

			blocks[gameloop::blocks::blockColumns - 1][gameloop::blocks::blockRows - 1] = gameloop::blocks::BLOCK_TYPE::UNBREAKABLE;
			gameloop::blocks::Init(blocks); 
		}

		void LevelManager()
		{
			LevelOne();
			LevelTwo();
			LevelThree();
		}
	}
}
