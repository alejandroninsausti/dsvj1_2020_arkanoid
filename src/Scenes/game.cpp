#include "game.h"

#include "raylib.h"

#include "Config/screen_size.h"
#include "Config/key_bindings.h"
#include "Menu/menu.h"

namespace arkanoid
{
	void InitKeys()
	{
		config::res = config::RESOLUTIONS::MID_RES;
		config::InitScreenSize(config::res);
		InitWindow(config::screenWidth, config::screenHeight, "Basic Arkanoid v0.1"); //open OpenGL window
		SetTargetFPS(60); //set max frame rate
		config::InitKeys();
	}

	void DeInit()
	{
		CloseWindow(); //close Game
	}

	void StartGame()
	{
		InitKeys();
		menu::menu_main::MainMenu();
		DeInit();
	}
}