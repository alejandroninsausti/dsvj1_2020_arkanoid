#ifndef MENU_H
#define MENU_H

#include "raylib.h"

namespace arkanoid
{
	namespace menu
	{
		bool OptionClicked(Rectangle textBox);
		void InitTextbox(Rectangle& textbox, int letterSize, float lengthOfString, 
			float xTenthPosition, float yTenthPosition);
		Rectangle InitTextTextbox(Rectangle textbox, int letterSize);

		namespace menu_main
		{
			void MainMenu();
		}
	}
}

#endif // !MENU_H