#include "menu_credits.h"

#include "raylib.h"

#include "Scenes/Menu/menu.h"
#include "Config/screen_size.h"
#include "Config/key_bindings.h"

namespace arkanoid
{
	namespace menu
	{
		namespace menu_credits
		{
			float letterSize;
			float letterSpacing;

			Rectangle textBoxExit;

			void Init()
			{
				letterSize = config::screenWidth * 0.03f;
				letterSpacing = letterSize / 10;

				InitTextbox(textBoxExit, letterSize * 2, 3.5f, 5, 9.1f);
			}

			void Draw()
			{
				BeginDrawing();

				//draw library credits
				Rectangle libraryTextbox =
				{
					0, //x position
					config::screenHeight - config::screenHeight / 10 * 9, //y position
					letterSize * 20, //width
					letterSize //height
				};
				DrawTextRec(GetFontDefault(), "Library used: RayLib", libraryTextbox, letterSize, letterSpacing, true, GRAY);

				//draw graphics motor credits
				Rectangle motorTextbox =
				{
					0, //x position
					config::screenHeight - config::screenHeight / 10 * 8, //y position
					letterSize * 26, //width
					letterSize //height
				};
				DrawTextRec(GetFontDefault(), "Graphic Motor used: OpenGL", motorTextbox, letterSize, letterSpacing, true, GRAY);

				//draw author credits
				Rectangle authorTextbox =
				{
					0, //x position
					config::screenHeight - config::screenHeight / 10 * 7, //y position
					letterSize * 26, //width
					letterSize //height
				};
				DrawTextRec(GetFontDefault(), "Made by Alejandro Insausti", authorTextbox, letterSize, letterSpacing, true, GRAY);

				//draw exit option
				DrawRectangleRec(textBoxExit, GRAY);
				Rectangle exitTextbox =
				{
					textBoxExit.x + letterSize * 2 / 5, //x position
					textBoxExit.y, //y position
					letterSize * 4 * 2, //width
					letterSize * 2 //height
				};
				DrawTextRec(GetFontDefault(), "Exit", exitTextbox, letterSize * 2, letterSpacing * 2, true, DARKGRAY);

				ClearBackground(DARKGRAY);
				EndDrawing();
			}

			void CreditsMenu()
			{
				Init();
				
				do
				{
					Draw();
				} while (IsKeyUp(config::keys.quit) &&
					(IsMouseButtonUp(MOUSE_LEFT_BUTTON) || !OptionClicked(textBoxExit)));
			}
		}
	}
}