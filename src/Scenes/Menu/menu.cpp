#include "menu.h"

#include "raylib.h"

#include "Config/screen_size.h"
#include "Scenes/Gameloop/gameloop.h"
#include "Scenes/Menu/Credits/menu_credits.h"
#include "Settings/menu_settings.h"
#include "Config/key_bindings.h""

namespace arkanoid
{
	namespace menu
	{
		bool OptionClicked(Rectangle textBox)
		{
			return CheckCollisionPointRec(GetMousePosition(), textBox);
		}

		void InitTextbox(Rectangle& textbox, int letterSize, float lengthOfString,
			float xTenthPosition, float yTenthPosition)
		{
			textbox.width = letterSize * (lengthOfString / 2 + 0.5f); //width
			textbox.height = letterSize * 1.1f; //height
			textbox.x = config::screenWidth / 10 * xTenthPosition - textbox.width / 2; //x position in a divided by 10 screen
			textbox.y = config::screenHeight / 10 * yTenthPosition; //y position in a divided by 10 screen
		}

		Rectangle InitTextTextbox(Rectangle textbox, int letterSize)
		{
			Rectangle textTextbox;

			textTextbox.width = textbox.width * 1.1f; //parent width with an extra
			textTextbox.height = textbox.height; //parent height
			textTextbox.x = textbox.x * 1.02f; //parent x position
			textTextbox.y = textbox.y; //parent y position

			return textTextbox;
		}

		namespace menu_main
		{
			int letterSize;
			int letterSpacing;
			int titleSize;

			Rectangle textBoxPlay;

			Rectangle textBoxSettings;

			Rectangle textBoxCredits;

			Rectangle textBoxExit;

			void Init()
			{
				letterSize = config::screenWidth * 0.045f;
				letterSpacing = letterSize / 10;
				titleSize = letterSize * 4;

				InitTextbox(textBoxPlay, letterSize, 4, 5, 5);
				InitTextbox(textBoxSettings, letterSize, 8, 5, 6.25f);
				InitTextbox(textBoxCredits, letterSize, 7.25f, 5, 7.5f);
				InitTextbox(textBoxExit, letterSize, 3.5f, 5, 8.75f);
			}

			void DrawOptions()
			{
				//draw play option
				DrawRectangleRec(textBoxPlay, GRAY);
				Rectangle playOptionTextbox = InitTextTextbox(textBoxPlay, letterSize);
				DrawTextRec(GetFontDefault(), "Play", playOptionTextbox, letterSize, letterSpacing, true, DARKGRAY);

				//draw settings option
				DrawRectangleRec(textBoxSettings, GRAY);
				Rectangle settingsOptionTextbox = InitTextTextbox(textBoxSettings, letterSize);
				DrawTextRec(GetFontDefault(), "Settings", settingsOptionTextbox, letterSize, letterSpacing, true, DARKGRAY);

				//draw credits option
				DrawRectangleRec(textBoxCredits, GRAY);
				Rectangle creditsOptionTextbox = InitTextTextbox(textBoxCredits, letterSize);
				DrawTextRec(GetFontDefault(), "Credits", creditsOptionTextbox, letterSize, letterSpacing, true, DARKGRAY);

				//draw exit option
				DrawRectangleRec(textBoxExit, GRAY);
				Rectangle exitOptionTextbox = InitTextTextbox(textBoxExit, letterSize);
				DrawTextRec(GetFontDefault(), "Exit", exitOptionTextbox, letterSize, letterSpacing, true, DARKGRAY);
			}

			void Draw()
			{
				BeginDrawing();
				//draw title
				DrawText("Arkanoid", static_cast<int>(config::screenWidth / 2 - 4 * titleSize / 2), 0, titleSize, GRAY);
				//draw play, settings and exit options
				DrawOptions();
				ClearBackground(DARKGRAY);
				EndDrawing();
			}

			void MainMenu()
			{
				Init();

				do
				{
					//Input | Update
					if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
					{
						if (OptionClicked(textBoxPlay))
						{
							gameloop::Gameloop();
						}
						else if (OptionClicked(textBoxSettings))
						{
							menu_settings::SettingsMenu();
							Init();
						}
						else if (OptionClicked(textBoxCredits))
						{
							menu_credits::CreditsMenu();
						}
					}

					//Draw
					Draw();
				} while (!IsKeyReleased(config::keys.quit) &&
					(!IsMouseButtonPressed(MOUSE_LEFT_BUTTON) || !OptionClicked(textBoxExit)));
			}
		}
	}
}