#include "menu_settings.h"

#include "raylib.h"

#include "Scenes/Menu/menu.h"
#include "Config/screen_size.h"
#include "Config/volume.h"
#include "Config/key_bindings.h"

namespace arkanoid
{
	namespace menu
	{
		namespace menu_settings
		{
			int letterSize;
			int letterSpacing;

			/*
			namespace settings_audio
			{
				Rectangle textBoxMasterOnOff;

				Rectangle textBoxMusicOnOff;

				Rectangle textBoxSoundOnOff;

				Rectangle textBoxExit;;

				void Init()
				{
					letterSize = config::screenWidth * 0.06f;
					letterSpacing = letterSize / 10;

					InitTextbox(textBoxMasterOnOff, letterSize, 3, 5, 2);
					InitTextbox(textBoxMusicOnOff, letterSize, 3, 2.5f, 5);
					InitTextbox(textBoxSoundOnOff, letterSize, 3, 7.5f, 5);
					InitTextbox(textBoxExit, letterSize, 3.5f, 5, 9);
				}

				void DrawMuteOption(Rectangle textBox, float volume)
				{
					Rectangle muteOptionTextbox = InitTextTextbox(textBox, letterSize);

					if (volume == 0)
					{
						DrawRectangleRec(textBox, RED);
						DrawTextRec(GetFontDefault(), "OFF", muteOptionTextbox, letterSize, letterSpacing, true, DARKPURPLE);
					}
					else
					{
						DrawRectangleRec(textBox, GREEN);
						DrawTextRec(GetFontDefault(), "ON", muteOptionTextbox, letterSize, letterSpacing, true, DARKGREEN);
					}
				}

				void DrawOptions()
				{
					//draw master volume option
					Rectangle textBoxMaster;
					InitTextbox(textBoxMaster, letterSize, 14, 5, 1);
					DrawMuteOption(textBoxMasterOnOff, config::masterVolume);
					DrawTextRec(GetFontDefault(), "Master Volume", textBoxMaster,
						letterSize, letterSpacing, true, GRAY);

					//draw music volume option
					Rectangle textBoxMusic;
					InitTextbox(textBoxMusic, letterSize, 13, 2.5f, 4);
					DrawMuteOption(textBoxMusicOnOff, config::musicVolume);
					DrawTextRec(GetFontDefault(), "Music Volume", textBoxMusic,
						letterSize, letterSpacing, true, GRAY);

					//draw sound volume option
					Rectangle textBoxSound;
					InitTextbox(textBoxSound, letterSize, 13, 7.5f, 4);
					DrawMuteOption(textBoxSoundOnOff, config::soundVolume);
					DrawTextRec(GetFontDefault(), "Sound Volume", textBoxSound,
						letterSize, letterSpacing, true, GRAY);

					//draw exit option
					DrawRectangleRec(textBoxExit, GRAY);
					Rectangle exitOptionTextbox = InitTextTextbox(textBoxExit, letterSize);
					DrawTextRec(GetFontDefault(), "Exit", exitOptionTextbox,
						letterSize, letterSpacing, true, DARKGRAY);
				}

				void Draw()
				{
					BeginDrawing();
					//draw play, settings and exit options
					DrawOptions();
					ClearBackground(DARKGRAY);
					EndDrawing();
				}

				void AudioSettings()
				{
					Init();

					do
					{
						//Input | Update
						if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
						{
							if (OptionClicked(textBoxMasterOnOff))
							{
								if (config::masterVolume == 1)
								{
									config::masterVolume = 0;
								}
								else
								{
									config::masterVolume = 1;
								}
								SetMasterVolume(config::masterVolume);
							}
							else if (OptionClicked(textBoxSoundOnOff))
							{
								if (config::soundVolume == 1)
								{
									config::soundVolume = 0;
								}
								else
								{
									config::soundVolume = 1;
								}
							}
							else if (OptionClicked(textBoxMusicOnOff))
							{
								if (config::musicVolume == 1)
								{
									config::musicVolume = 0;
								}
								else
								{
									config::musicVolume = 1;
								}
							}
						}

						//Draw
						Draw();
					} while (IsKeyUp(config::keys.quit) &&
						(IsMouseButtonUp(MOUSE_LEFT_BUTTON) || !OptionClicked(textBoxExit)));
				}
			}
			*/

			namespace settings_graphics
			{
				Rectangle textBoxActualResolution;

				Rectangle textBoxFullscreenOnOff;

				Rectangle textBoxExit;

				void Init()
				{
					letterSize = config::screenWidth * 0.06f;
					letterSpacing = letterSize / 10;

					InitTextbox(textBoxActualResolution, letterSize, 8, 2.5f, 4);
					InitTextbox(textBoxFullscreenOnOff, letterSize, 3.5f, 7.5f, 4);
					InitTextbox(textBoxExit, letterSize, 3.5f, 5, 9);
				}

				void DrawActualResolution()
				{
					Rectangle resOptionTextbox = InitTextTextbox(textBoxActualResolution, letterSize);

					DrawRectangleRec(textBoxActualResolution, GRAY);
					switch (config::res)
					{
					case arkanoid::config::RESOLUTIONS::LOW_RES:
						DrawTextRec(GetFontDefault(), "720x480", resOptionTextbox,
							letterSize, letterSpacing, true, ORANGE);
						break;
					case arkanoid::config::RESOLUTIONS::MID_RES:
						DrawTextRec(GetFontDefault(), "800x600", resOptionTextbox, 
							letterSize, letterSpacing, true, ORANGE);
						break;
					case arkanoid::config::RESOLUTIONS::HIGH_RES:
						DrawTextRec(GetFontDefault(), "1152x900", resOptionTextbox,
							letterSize, letterSpacing, true, ORANGE);
						break;
					default:
						break;
					}
				}

				void DrawFullscreenOnOffOption()
				{
					Rectangle fullOptionTextbox = InitTextTextbox(textBoxFullscreenOnOff, letterSize);

					if (IsWindowFullscreen())
					{
						DrawRectangleRec(textBoxFullscreenOnOff, GREEN);
						DrawTextRec(GetFontDefault(), "ON", fullOptionTextbox, letterSize, letterSpacing, true, DARKGREEN);
					}
					else
					{
						DrawRectangleRec(textBoxFullscreenOnOff, RED);
						DrawTextRec(GetFontDefault(), "OFF", fullOptionTextbox, letterSize, letterSpacing, true, DARKPURPLE);
					}
				}

				void DrawOptions()
				{
					//draw Resolution option
					Rectangle textBoxResolution;
					InitTextbox(textBoxResolution, letterSize, 10, 2.5f, 3);
					DrawActualResolution();
					DrawTextRec(GetFontDefault(), "Resolution", textBoxResolution, letterSize, letterSpacing, true, GRAY);

					//draw Fullscreen option
					Rectangle textBoxFullscreen;
					InitTextbox(textBoxFullscreen, letterSize, 10, 7.5f, 3);
					DrawFullscreenOnOffOption();
					DrawTextRec(GetFontDefault(), "Fullscreen", textBoxFullscreen, letterSize, letterSpacing, true, GRAY);

					//draw exit option
					DrawRectangleRec(textBoxExit, GRAY);
					Rectangle exitOptionTextbox = InitTextTextbox(textBoxExit, letterSize);
					DrawTextRec(GetFontDefault(), "Exit", exitOptionTextbox, letterSize, letterSpacing, true, DARKGRAY);
				}

				void Draw()
				{
					BeginDrawing();
					//draw play, settings and exit options
					DrawOptions();
					ClearBackground(DARKGRAY);
					EndDrawing();
				}

				void GraphicsSettings()
				{
					Init();

					do
					{
						//Input | Update
						if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
						{
							if (OptionClicked(textBoxActualResolution))
							{
								switch (config::res)
								{
								case arkanoid::config::RESOLUTIONS::LOW_RES:
									config::res = arkanoid::config::RESOLUTIONS::MID_RES;
									config::InitScreenSize(config::res);
									break;
								case arkanoid::config::RESOLUTIONS::MID_RES:
									config::res = arkanoid::config::RESOLUTIONS::HIGH_RES;
									config::InitScreenSize(config::res);
									break;
								case arkanoid::config::RESOLUTIONS::HIGH_RES:
									config::res = arkanoid::config::RESOLUTIONS::LOW_RES;
									config::InitScreenSize(config::res);
									break;
								default:
									break;
								}
								Init();
							}
							else if (OptionClicked(textBoxFullscreenOnOff))
							{
								ToggleFullscreen();
							}
						}

						//Draw
						Draw();
					} while (IsKeyUp(config::keys.quit) &&
						(IsMouseButtonUp(MOUSE_LEFT_BUTTON) || !OptionClicked(textBoxExit)));
				}
			}

			//Rectangle textBoxAudio;

			Rectangle textBoxGraphics;

			Rectangle textBoxExit;

			const float exitLetterModifier = 0.75f;

			void Init()
			{
				letterSize = config::screenWidth * 0.075f;
				letterSpacing = letterSize / 10;

				//InitTextbox(textBoxAudio, letterSize, 5, 5, 3);
				InitTextbox(textBoxGraphics, letterSize, 8, 5, 5);
				InitTextbox(textBoxExit, letterSize * exitLetterModifier, 3.5f, 5, 9);
			}

			void DrawOptions()
			{
				/*
				//draw audio option
				DrawRectangleRec(textBoxAudio, GRAY);
				Rectangle audioOptionTextbox = InitTextTextbox(textBoxAudio, letterSize);
				DrawTextRec(GetFontDefault(), "Audio", audioOptionTextbox,
					letterSize, letterSpacing, true, DARKGRAY);
				*/
				//draw graphics option
				DrawRectangleRec(textBoxGraphics, GRAY);
				Rectangle graphicsOptionTextbox = InitTextTextbox(textBoxGraphics, letterSize);
				DrawTextRec(GetFontDefault(), "Graphics", graphicsOptionTextbox,
					letterSize, letterSpacing, true, DARKGRAY);

				//draw exit option
				DrawRectangleRec(textBoxExit, GRAY);
				Rectangle exitOptionTextbox = InitTextTextbox(textBoxExit, letterSize * exitLetterModifier);
				DrawTextRec(GetFontDefault(), "Exit", exitOptionTextbox,
					letterSize * exitLetterModifier, letterSpacing, true, DARKGRAY);
			}

			void Draw()
			{
				BeginDrawing();
				//draw all the settings and the exit options
				DrawOptions();
				ClearBackground(DARKGRAY);
				EndDrawing();
			}

			void SettingsMenu()
			{
				Init();
				do
				{
					//Input | Update
					if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
					{
						/*if (OptionClicked(textBoxAudio))
						{
							settings_audio::AudioSettings();
						}
						else
						*/
						if (OptionClicked(textBoxGraphics))
						{
							settings_graphics::GraphicsSettings();
							Init();
						}
					}

					//Draw
					Draw();
				} while (!IsKeyReleased(config::keys.quit) &&
					(!IsMouseButtonPressed(MOUSE_LEFT_BUTTON) || !OptionClicked(textBoxExit)));
			}
		}
	}
}


