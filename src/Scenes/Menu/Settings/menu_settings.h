#ifndef MENU_SETTINGS_H
#define MENU_SETTINGS_H

#include "menu_settings.h"

#include "raylib.h"

#include "Scenes/Menu/menu.h"
#include "Config/screen_size.h"

namespace arkanoid
{
	namespace menu
	{
		namespace menu_settings
		{
			void SettingsMenu();
		}
	}
}

#endif // !MENU_SETTINGS_H
