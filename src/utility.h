#ifndef UTILITY_H
#define UTILITY_H

#include "raylib.h"

bool IntIsWithinRange(int input, int min, int max);
float GetAbsoluteValue(float number);
int InputKey();
char ChangeIntNumberToCharNumber(int number);
void InitTextbox(Rectangle& textbox, int letterSize, float lengthOfString,
	float xTenthPosition, float yTenthPosition);

#endif // !UTILITY_H

