#ifndef VOLUME_H
#define VOLUME_H

namespace arkanoid
{
	namespace config
	{
		float masterVolume = 1;
		float musicVolume = 1;
		float soundVolume = 1;
	}
}

#endif // !VOLUME_H
