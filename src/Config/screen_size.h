#ifndef SCREEN_SIZE_H
#define SCREEN_SIZE_H

namespace arkanoid
{
	namespace config
	{
		enum class RESOLUTIONS { LOW_RES, MID_RES, HIGH_RES };

		void InitScreenSize(RESOLUTIONS newRes);

		extern RESOLUTIONS res;
		extern int screenWidth;
		extern int screenHeight;
	}
}

#endif // !SCREEN_SIZE_H