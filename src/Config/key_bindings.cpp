#include "key_bindings.h"

#include "raylib.h"

namespace arkanoid
{
	namespace config
	{
		KeyBindings keys;

		void InitKeys()
		{
			keys.moveLeft = KEY_LEFT;
			keys.moveRight = KEY_RIGHT;
			keys.quit = KEY_ESCAPE;
			keys.releaseBall = KEY_SPACE;
		}
	}
}