#include "screen_size.h"

#include "raylib.h"

namespace arkanoid
{
	namespace config
	{
		RESOLUTIONS res;
		int screenWidth;
		int screenHeight;

		void InitScreenSize(RESOLUTIONS newRes)
		{
			res = newRes;

			switch (res)
			{
			case arkanoid::config::RESOLUTIONS::LOW_RES:
				screenWidth = 720;
				screenHeight = 480;
				break;
			case arkanoid::config::RESOLUTIONS::MID_RES:
				screenWidth = 800;
				screenHeight = 600;
				break;
			case arkanoid::config::RESOLUTIONS::HIGH_RES:
				screenWidth = 1152;
				screenHeight = 900;
				break; 
			default:
				break;
			}

			SetWindowSize(screenWidth, screenHeight);
		}
	}
}