#ifndef KEY_BINDINGS_H
#define KEY_BINDINGS_H

namespace arkanoid
{
	namespace config
	{
		struct KeyBindings
		{
			int moveLeft;
			int moveRight;
			int releaseBall;
			int quit;
		};

		void InitKeys();

		extern KeyBindings keys;
	}
}

#endif // !KEY_BINDINGS_H
