#ifndef PLAYER_DATA_H
#define PLAYER_DATA_H

namespace arkanoid
{
	namespace save
	{
		struct PlayerData
		{
			int score;
			char name[3];
		};
	}
}

#endif // !PLAYER_DATA_H